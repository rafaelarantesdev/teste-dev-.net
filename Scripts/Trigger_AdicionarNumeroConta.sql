CREATE TRIGGER AdicionarNumeroConta
ON Conta
AFTER INSERT
AS
BEGIN
	DECLARE @Agencia int
	DECLARE @Numero int

	SELECT @Agencia = Agencia FROM INSERTED
	
	SELECT @Numero = max(Numero)+1 FROM Conta
	WHERE Agencia = @Agencia

	if @Numero = 1
		set @Numero = 100001

	UPDATE c SET Numero = @Numero
	FROM Conta c
	INNER JOIN inserted i ON c.Id = i.Id

END;


