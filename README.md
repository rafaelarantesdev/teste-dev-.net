# Interview GSW

O projeto **CE (Caixa Eletrônico)** foi desenvolvido utilizando dois contextos principais:
 1.  **Cadastro** - Responsável pelo cadastro de clientes
 2.  **Caixa** - Responsável pelos saques.

Também foi utilizado um contexto compartilhado **(Shared)** para autenticação de usuários do sistema e clientes.

## Especificações

1. Visual Studio 2017
2. SQL Server 2017

## Instruções


1. No **Visual Studio** ir em **Restore Nuget Packages**.
2. No projeto **GSW.CE.Presentation.API > appsettings.json** configurar a conexão do **SQL Server**.
3. Depois em **Package Manager Console**:
	
    3.1 - Executar **update-database -Context CadastroContext** apontando para o projeto **GSW.CE.Cadastro.Data**.
	
    3.2 - Executar **update-database -Context AutenticacaoContext** apontando para o projeto **GSW.CE.Shared.Data**.
4. Na pasta **Scripts** na raiz do projeto estão os scripts que deveram ser executados no **banco de dados (CE)**, não importando a ordem de execução.

Obs: Não é necessário executar o **update-database** no **CaixaContext** pois utiliza do mesmo mapeamento do **CadastroContext**.


