﻿using GSW.CE.Caixa.Data.Repository.Interface;
using GSW.CE.Caixa.Domain.Entity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace GSW.CE.Caixa.Data.Repository
{
    public class CaixaRepository : ICaixaRepository
    {
        private readonly CaixaContext _caixaContext;
        public CaixaRepository(CaixaContext caixaContext)
        {
            _caixaContext = caixaContext;
        }

        public async Task<Cliente> ObterClientePeloId(Guid clienteId)
        {
            return await _caixaContext.Clientes.AsNoTracking().Include(x => x.Conta).FirstOrDefaultAsync(x => x.Id == clienteId);
        }

        public async Task SaveAsync(Conta conta)
        {
            if (conta.Id == Guid.Empty)
            {
                _caixaContext.Contas.Add(conta);
            }
            else
            {
                _caixaContext.Contas.Update(conta);
            }

            await _caixaContext.SaveChangesAsync();
        }
    }
}
