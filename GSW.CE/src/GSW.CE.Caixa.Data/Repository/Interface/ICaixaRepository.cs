﻿using System;
using System.Threading.Tasks;
using GSW.CE.Core.Data.Interface;
using GSW.CE.Caixa.Domain.Entity;

namespace GSW.CE.Caixa.Data.Repository.Interface
{
    public interface ICaixaRepository : IRepository<Conta>
    {
        Task<Cliente> ObterClientePeloId(Guid clienteId);
    }
}
