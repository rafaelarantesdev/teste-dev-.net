﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace GSW.CE.Caixa.Data.Migrations
{
    public partial class Inicial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Conta",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Agencia = table.Column<string>(type: "varchar(5)", nullable: false),
                    Numero = table.Column<int>(type: "int", nullable: false),
                    Senha = table.Column<string>(type: "varchar(32)", nullable: true),
                    Saldo = table.Column<decimal>(type: "money", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Conta", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Cliente",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Nome = table.Column<string>(type: "varchar(50)", nullable: true),
                    Sobrenome = table.Column<string>(type: "varchar(50)", nullable: true),
                    DataNascimento = table.Column<DateTime>(type: "date", nullable: false),
                    CPF = table.Column<string>(type: "varchar(11)", nullable: true),
                    ContaId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Cliente", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Cliente_Conta_ContaId",
                        column: x => x.ContaId,
                        principalTable: "Conta",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Cliente_ContaId",
                table: "Cliente",
                column: "ContaId",
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Cliente");

            migrationBuilder.DropTable(
                name: "Conta");
        }
    }
}
