﻿using GSW.CE.Caixa.Domain.Entity;
using Microsoft.EntityFrameworkCore;

namespace GSW.CE.Caixa.Data
{
    public class CaixaContext : DbContext
    {
        public CaixaContext(DbContextOptions<CaixaContext> options) : base(options) { }

        public DbSet<Cliente> Clientes { get; set; }
        public DbSet<Conta> Contas { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfigurationsFromAssembly(typeof(CaixaContext).Assembly);
        }
    }
}
