﻿using GSW.CE.Caixa.Domain.Entity;
using GSW.CE.Shared.Data.Mapping;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace GSW.CE.Caixa.Data.Mapping
{
    public class ClienteMapping : ClienteBaseMapping<Cliente>, IEntityTypeConfiguration<Cliente>
    {
        public override void AddCustomConfig(EntityTypeBuilder<Cliente> builder)
        {
            builder.HasOne(x => x.Conta)
                .WithOne(x => x.Cliente)
                .HasForeignKey<Cliente>(x => x.ContaId);

            base.AddCustomConfig(builder);
        }
    }
}
