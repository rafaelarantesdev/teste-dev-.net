﻿using GSW.CE.Cadastro.Data.Mapping;
using GSW.CE.Caixa.Domain.Entity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace GSW.CE.Caixa.Data.Mapping
{
    public class ContaMapping : ContaBaseMapping<Conta>, IEntityTypeConfiguration<Conta>
    {
        public override void AddCustomConfig(EntityTypeBuilder<Conta> builder)
        {
            builder.HasOne(x => x.Cliente)
               .WithOne(x => x.Conta)
               .HasForeignKey<Cliente>(x => x.ContaId);

            base.AddCustomConfig(builder);
        }
    }
}
