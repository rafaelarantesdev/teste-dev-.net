﻿
using System;

namespace GSW.CE.Shared.Domain.ValueObject
{
    public class CPF
    {
        public string Numero { get; private set; }
        public CPF(string numero)
        {
            Numero = numero.Replace(".", "").Replace("-", "");
        }
    }
}
