﻿
using System;

namespace GSW.CE.Shared.Domain.ValueObject
{
    public class Saldo
    {
        public decimal Valor { get; private set; }

        public Saldo(decimal valor)
        {
            Valor = valor;
        }

        public void Debitar(decimal valor)
        {
            Valor -= valor;
        }

        public void Depositar(decimal valor)
        {
            Valor += valor;
        }
    }
}
