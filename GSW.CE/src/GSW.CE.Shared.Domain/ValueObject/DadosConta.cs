﻿
using FluentValidation;
using GSW.CE.Core.Cryptography;
using System;
using System.Security.Cryptography;

namespace GSW.CE.Shared.Domain.ValueObject
{
    public class DadosConta
    {
        public int Agencia { get; private set; }
        public int Numero { get; private set; }
        public string Senha { get; private set; }
        public Guid ContaId { get; set; }

        protected DadosConta() {}

        public DadosConta(int agencia)
        {
            Agencia = agencia;
            Numero = 000000;
            Senha = "";
        }


        public void AssociarConta(Guid contaId)
        {
            ContaId = contaId;
        }

        public DadosConta AdicionarNumeroConta(int numero)
        {
            if(Numero == 0)
                Numero = numero;

            return this;
        }

        public DadosConta AlterarSenha(string senha)
        {
            if(string.IsNullOrEmpty(senha) && string.IsNullOrEmpty(Senha))
            {
                throw new ValidationException("A senha deve ser preenchida");
            }
            else
            {
                if (senha.ToString().Length != 6)
                    throw new ValidationException("A senha precisa ter 6 dígitos");

                Senha = MD5Crypt.Generate(senha.ToString());
            }

            return this;
        }
    }
}
