﻿using System;

namespace GSW.CE.Shared.Domain.ValueObject
{
    public class DataNascimento
    {
        public DateTime Data { get; private set; }

        public DataNascimento(DateTime data)
        {
            Data = data;
        }
    }
}
