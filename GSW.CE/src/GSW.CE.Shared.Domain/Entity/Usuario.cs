﻿
using FluentValidation.Results;
using GSW.CE.Core.DomainObject;
using GSW.CE.Shared.Domain.Entity.Validation;
using System;

namespace GSW.CE.Shared.Domain.Entity
{
    public class Usuario : EntityBase
    {
        public string Login { get; private set; }
        public string Senha { get; private set; }
        public string Permissao { get; private set; }
        private string _token;

        public Usuario(string login = "", string permissao = "Conta")
        {
            Login = !string.IsNullOrEmpty(login) ? login : Id.ToString();
            Permissao = permissao;
        }

        public void AdicionarToken(string token)
        {
            _token = token;
        }

        public string ObterToken()
        {
            return _token;
        }

        public void AlterarSenha(string senha)
        {
            Senha = senha;
        }

        public void AlterarLogin(string login)
        {
            Login = login;
        }

        public override ValidationResult Validar()
        {
            return new UsuarioValidation().Validate(this);
        }
    }
}
