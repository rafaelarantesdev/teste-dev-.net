﻿using FluentValidation;

namespace GSW.CE.Shared.Domain.Entity.Validation
{
    public abstract class ContaBaseValidation : AbstractValidator<ContaBase>
    {
        public ContaBaseValidation()
        {
            RuleFor(x => x.DadosConta.Agencia.ToString())
                .Length(5)
                .WithMessage("A Agencia precisa ter 5 digitos");

            RuleFor(x => x.DadosConta.Numero.ToString())
                .Length(6)
                .WithMessage("O Número da Conta precisa ter 6 digitos");

            AddCustomConfig();
        }

        public virtual void AddCustomConfig()
        {
        }
    }
}
