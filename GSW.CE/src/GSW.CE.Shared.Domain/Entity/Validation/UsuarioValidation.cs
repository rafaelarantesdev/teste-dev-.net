﻿using FluentValidation;

namespace GSW.CE.Shared.Domain.Entity.Validation
{
    public class UsuarioValidation : AbstractValidator<Usuario>
    {
        public UsuarioValidation()
        {
            RuleFor(x => x.Login)
                .NotEmpty()
                .WithMessage("O Login não pode estar vazio");

            RuleFor(x => x.Permissao)
                .NotEmpty()
                .WithMessage("A permissão não pode estar vazio");

            RuleFor(x => x.Senha)
                .NotEmpty()
                .WithMessage("A senha não pode estar vazia");
        }
    }
}
