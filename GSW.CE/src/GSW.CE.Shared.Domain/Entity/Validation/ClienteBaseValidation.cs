﻿using CpfLibrary;
using FluentValidation;
using System;

namespace GSW.CE.Shared.Domain.Entity.Validation
{
    public abstract class ClienteBaseValidation : AbstractValidator<ClienteBase>
    {
        public ClienteBaseValidation()
        {
            RuleFor(x => x.CPF)
                .Must(x => Cpf.Check(x.Numero))
                .WithMessage("CPF inválido");

            RuleFor(x => x.DataNascimento.Data)
                .NotNull()
                .GreaterThan(DateTime.Parse("1880-01-01"))
                .LessThan(DateTime.Now.AddSeconds(1))
                .WithMessage("Data de nascimento inválida");

            RuleFor(x => x.NomeCompleto.Nome)
                .NotEmpty()
                .WithMessage("Nome precisa ser preenchido");

            RuleFor(x => x.NomeCompleto.Sobrenome)
                .NotEmpty()
                .WithMessage("Sobrenome precisa ser preenchido");
        }

        public virtual void AddCustomConfig()
        {
        }
    }
}
