﻿
using GSW.CE.Core.DomainObject;
using GSW.CE.Shared.Domain.ValueObject;

namespace GSW.CE.Shared.Domain.Entity
{
    public abstract class ContaBase : EntityBase
    {
        public DadosConta DadosConta { get; protected set; }
        public Saldo Saldo { get; protected set; }
    }
}
