﻿using GSW.CE.Core.DomainObject;
using GSW.CE.Shared.Domain.ValueObject;
using System;

namespace GSW.CE.Shared.Domain.Entity
{
    public abstract class ClienteBase : EntityBase
    {
        public NomeCompleto NomeCompleto { get; protected set; }
        public DataNascimento DataNascimento { get; protected set; }
        public CPF CPF { get; protected set; }
        public Guid ContaId { get; protected set; }
    }
}
