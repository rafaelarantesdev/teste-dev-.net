﻿using FluentValidation.Results;
using GSW.CE.Cadastro.Domain.Entity.Validation;
using GSW.CE.Shared.Domain.Entity;
using GSW.CE.Shared.Domain.ValueObject;
using System;

namespace GSW.CE.Cadastro.Domain.Entity
{
    public class Cliente : ClienteBase
    {
        public Conta Conta { get; private set; }

        protected Cliente() {}

        public Cliente(NomeCompleto nomeCompleto, DataNascimento dataNascimento, CPF cpf, Conta conta)
        {
            conta.AssociarCliente(this);
            NomeCompleto = nomeCompleto;
            DataNascimento = dataNascimento;
            CPF = cpf;
            Conta = conta;
            ContaId = Conta.Id;
        }

        public override ValidationResult Validar()
        {
            return new ClienteValidation().Validate(this);
        }
    }

}
