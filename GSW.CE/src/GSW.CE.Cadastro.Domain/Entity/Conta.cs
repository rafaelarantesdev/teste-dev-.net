﻿using FluentValidation.Results;
using GSW.CE.Cadastro.Domain.Entity.Validation;
using GSW.CE.Shared.Domain.Entity;
using GSW.CE.Shared.Domain.ValueObject;
using System;

namespace GSW.CE.Cadastro.Domain.Entity
{
    public class Conta : ContaBase
    {
        protected Conta() {}

        public Cliente Cliente { get; private set; }
        public Guid UsuarioId { get; private set; }
        

        public Conta(Guid id, DadosConta dadosConta, Saldo saldo)
        {
            if (id != Guid.Empty)
                Id = id;
                
            dadosConta.AssociarConta(Id);
            DadosConta = dadosConta;
            Saldo = saldo;
        }

        public void AssociarCliente(Cliente cliente)
        {
            Cliente = cliente;
        }

        public void AssociarUsuario(Guid usuarioId)
        {
            UsuarioId = usuarioId;
        }

        public override ValidationResult Validar()
        {
            return new ContaValidation().Validate(this);
        }
    }
}
