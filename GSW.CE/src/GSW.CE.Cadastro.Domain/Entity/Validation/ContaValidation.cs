﻿
using FluentValidation;
using GSW.CE.Shared.Domain.Entity.Validation;

namespace GSW.CE.Cadastro.Domain.Entity.Validation
{
    public class ContaValidation : ContaBaseValidation
    {
        public override void AddCustomConfig()
        {
            RuleFor(x => x.Saldo.Valor)
                    .GreaterThanOrEqualTo(0)
                    .WithMessage("O saldo não pode ser negativo");

            base.AddCustomConfig();
        }
    }
}
