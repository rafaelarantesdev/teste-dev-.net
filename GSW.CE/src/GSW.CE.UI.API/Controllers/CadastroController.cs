﻿using FluentValidation;
using GSW.CE.Cadastro.Application.Service.Interface;
using GSW.CE.Cadastro.Application.ViewModel;
using GSW.CE.Presentation.API.ViewModel;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SharpApiRateLimit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GSW.CE.Presentation.API.Controllers
{
    [Produces("application/json")]
    [Route("api/Cadastro")]
    [RateLimitByHeader("X-UserId", "1s", calls: 5)]
    public class CadastroController : Controller
    {
        private readonly IClienteAppService _clienteAppService;
        public CadastroController(IClienteAppService clienteAppService)
        {
            _clienteAppService = clienteAppService;
        }

        /// <summary>
        /// Inclusão e Alteração de Cliente.
        /// </summary>
        [HttpPost]
        [Authorize("Admin")]
        [Route("Cliente")]
        public async Task<JsonResult> Salvar([FromBody]GravarClienteVModel clienteVModel)
        {
            try
            {
                await _clienteAppService.Salvar(clienteVModel);
                return Json(new ResultVModel() { Success = true });
            }
            catch(ValidationException vEx)
            {
                return Json(new ResultVModel()
                {
                    Success = false,
                    Messages = vEx.Errors.Count() > 0 ? vEx.Errors.Select(x => x.ToString()) : new List<string> { vEx.Message }
                });
            }
            catch(Exception ex)
            {
                return Json(new ResultVModel() { Success = false, Messages = new List<string> {"Erro interno."} });
            }
        }

        /// <summary>
        /// Obter todos os Clientes
        /// </summary>
        [HttpGet]
        [Authorize("Admin")]
        [Route("Cliente")]
        public async Task<JsonResult> ObterClientes()
        {
            try
            {
                var clientes = await _clienteAppService.ObterClientes();
                return Json(new ResultVModel() { Success = true, Data = clientes });
            }
            catch (ValidationException vEx)
            {
                return Json(new ResultVModel()
                {
                    Success = false,
                    Messages = vEx.Errors.Count() > 0 ? vEx.Errors.Select(x => x.ToString()) : new List<string> { vEx.Message }
                });
            }
            catch (Exception ex)
            {
                return Json(new ResultVModel() { Success = false, Messages = new List<string> { "Erro interno." } });
            }
        }

        /// <summary>
        /// Obter um cliente pelo Id
        /// </summary>
        [HttpGet]
        [Authorize("Admin")]
        [Route("Cliente/{clienteId:guid}")]
        public async Task<JsonResult> ObterCliente(Guid clienteId)
        {
            try
            {
                var clientes = await _clienteAppService.ObterCliente(clienteId);
                return Json(new ResultVModel() { Success = true, Data = clientes });
            }
            catch (ValidationException vEx)
            {
                return Json(new ResultVModel()
                {
                    Success = false,
                    Messages = vEx.Errors.Count() > 0 ? vEx.Errors.Select(x => x.ToString()) : new List<string> { vEx.Message }
                });
            }
            catch (Exception ex)
            {
                return Json(new ResultVModel() { Success = false, Messages = new List<string> { "Erro interno." } });
            }
        }
    }
}