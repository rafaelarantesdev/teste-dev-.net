﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FluentValidation;
using GSW.CE.Caixa.Application.Service.Interface;
using GSW.CE.Caixa.Application.ViewModel;
using GSW.CE.Presentation.API.ViewModel;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SharpApiRateLimit;

namespace GSW.CE.Presentation.API.Controllers
{
    [Produces("application/json")]
    [Route("api/CaixaEletronico")]
    [RateLimitByHeader("X-UserId", "1s", calls: 5)]
    public class CaixaEletronicoController : Controller
    {
        private readonly ICaixaAppService _caixaAppService;

        public CaixaEletronicoController(ICaixaAppService caixaAppService)
        {
            _caixaAppService = caixaAppService;
        }

        /// <summary>
        /// Realização de saques
        /// </summary>
        [HttpPost]
        [Authorize("Conta")]
        [Route("Saque")]
        public async Task<JsonResult> Sacar([FromBody]CaixaVModel saqueVModel)
        {
            try
            {
                var notas = (await _caixaAppService.Sacar(saqueVModel)).Select(x => x.ObterValor());
                return Json(new ResultVModel() { Data = notas, Success = true });
            }
            catch (ValidationException vEx)
            {
                return Json(new ResultVModel() {
                    Success = false,
                    Messages = vEx.Errors.Count() > 0 ? vEx.Errors.Select(x => x.ToString()) : new List<string> { vEx.Message }
                });
            }
            catch (Exception ex)
            {
                return Json(new ResultVModel() { Success = false, Messages = new List<string> { "Erro interno." } });
            }
        }

        /// <summary>
        /// Obtenção dos dados do cliente
        /// </summary>
        [HttpGet]
        [Authorize("Conta")]
        [Route("Cliente/{clienteId:guid}")]
        public async Task<JsonResult> ObterDadosCliente(Guid clienteId)
        {
            try
            {
                var dadosConta = await _caixaAppService.ObterDadosCliente(clienteId);
                return Json(new ResultVModel() { Data = dadosConta, Success = true });
            }
            catch (ValidationException vEx)
            {
                return Json(new ResultVModel()
                {
                    Success = false,
                    Messages = vEx.Errors.Count() > 0 ? vEx.Errors.Select(x => x.ToString()) : new List<string> { vEx.Message }
                });
            }
            catch (Exception ex)
            {
                return Json(new ResultVModel() { Success = false, Messages = new List<string> { "Erro interno." } });
            }
        }
    }
}