﻿using System;
using System.Text;
using System.Threading.Tasks;
using FluentValidation;
using GSW.CE.Presentation.API.ViewModel;
using GSW.CE.Shared.Application.Service.Interface;
using GSW.CE.Shared.Application.ViewModel;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using SharpApiRateLimit;

namespace GSW.CE.Presentation.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [RateLimitByHeader("X-UserId", "1s", calls: 5)]
    public class AutenticacaoController : Controller
    {
        private readonly IAutenticacaoAppService _autenticacaoAppService;
        private readonly IConfiguration _configuration;

        public AutenticacaoController(IAutenticacaoAppService autenticacaoAppService, IConfiguration configuration)
        {
            _autenticacaoAppService = autenticacaoAppService;
            _configuration = configuration;
        }

        /// <summary>
        /// Autenticação de qualquer usuário do sistema
        /// </summary>
        [HttpPost]
        [Route("Usuario")]
        [AllowAnonymous]
        public async Task<IActionResult> LoginUsuario([FromBody]AutenticacaoUsuarioVModel loginVModel)
        {
            try
            {
                var securityKey = Encoding.ASCII.GetBytes(_configuration.GetSection("SecurityKey").Value);

                var usuario = await _autenticacaoAppService.Login(loginVModel.Login, loginVModel.Senha, securityKey);

                if (usuario == null)
                    throw new ValidationException("Usuário ou Senha inválida");

                return Json(new AuthViewModel()
                {
                    Success = true,
                    Token = usuario.ObterToken()
                });
            }
            catch (ValidationException vEx)
            {
                return BadRequest(vEx.Message);
            }
            catch (Exception ex)
            {
                return BadRequest("Erro interno");
            }
        }

        /// <summary>
        /// Autenticação especificamente do Cliente
        /// </summary>
        [HttpPost]
        [AllowAnonymous]
        [Route("Cliente")]
        public async Task<IActionResult> LoginConta([FromBody]AutenticacaoContaVModel contaVModel)
        {
            try
            {
                var loginVModel = new AutenticacaoUsuarioVModel()
                {
                    Login = $"{ contaVModel.Agencia }{ contaVModel.Conta }",
                    Senha = contaVModel.Senha.ToString()
                };

                return await LoginUsuario(loginVModel);
            }
            catch (ValidationException vEx)
            {
                return BadRequest(vEx.Message);
            }
            catch (Exception ex)
            {
                return BadRequest("Erro interno");
            }
        }
    }
}