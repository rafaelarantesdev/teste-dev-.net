﻿using AutoMapper;
using GSW.CE.Cadastro.Application.AutoMapper;
using GSW.CE.Cadastro.Application.Service;
using GSW.CE.Cadastro.Application.Service.Interface;
using GSW.CE.Cadastro.Data;
using GSW.CE.Cadastro.Data.Repository;
using GSW.CE.Cadastro.Data.Repository.Interface;
using GSW.CE.Caixa.Application.AutoMapper;
using GSW.CE.Caixa.Application.Service;
using GSW.CE.Caixa.Application.Service.Interface;
using GSW.CE.Caixa.Data;
using GSW.CE.Caixa.Data.Repository;
using GSW.CE.Caixa.Data.Repository.Interface;
using GSW.CE.Shared.Application.Service;
using GSW.CE.Shared.Application.Service.Interface;
using GSW.CE.Shared.Data.Context;
using GSW.CE.Shared.Data.Repository;
using GSW.CE.Shared.Data.Repository.Interface;
using Microsoft.Extensions.DependencyInjection;

namespace GSW.CE.UI.API.Setup
{
    public static class IoC
    {
        public static void RegisterServices(this IServiceCollection services)
        {
            RegisterServicesAutoMapper(services);          

            services.AddScoped<IClienteAppService, ClienteAppService>();
            services.AddScoped<IClienteRepository, ClienteRepository>();
            services.AddScoped<CadastroContext>();

            services.AddScoped<ICaixaAppService, CaixaAppService>();
            services.AddScoped<ICaixaRepository, CaixaRepository>();
            services.AddScoped<CaixaContext>();

            services.AddScoped<IAutenticacaoAppService, AutenticacaoAppService>();
            services.AddScoped<IAutenticacaoRepository, AutenticacaoRepository>();
            services.AddScoped<AutenticacaoContext>();
        }

        private static void RegisterServicesAutoMapper(IServiceCollection services)
        {
            services.AddAutoMapper(typeof(CadastroDomainToViewModelMappingProfile), typeof(CadastroViewModelToDomainMappingProfile), typeof(CaixaDomainToViewModelMappingProfile));
        }
    }
}
