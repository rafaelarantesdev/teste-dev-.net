﻿
namespace GSW.CE.Presentation.API.ViewModel
{
    public class AuthViewModel
    {
        public bool Success { get; set; }
        public string Token { get; set; }
    }
}
