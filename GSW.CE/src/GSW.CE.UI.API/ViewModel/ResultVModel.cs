﻿
using System.Collections.Generic;

namespace GSW.CE.Presentation.API.ViewModel
{
    public class ResultVModel
    {
        public bool Success { get; set; }
        public IEnumerable<string> Messages { get; set; }
        public object Data { get; set; }
    }
}
