﻿using GSW.CE.Cadastro.Domain.Entity;
using GSW.CE.Core.Data.Interface;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GSW.CE.Cadastro.Data.Repository.Interface
{
    public interface IClienteRepository : IRepository<Cliente>
    {
        Task<Conta> ObterContaPeloCliente(Guid clienteId);
        Task<IEnumerable<Cliente>> ObterClientes();
        Task<Cliente> ObterCliente(Guid clienteId);
    }
}
