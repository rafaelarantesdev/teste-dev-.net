﻿using GSW.CE.Cadastro.Data.Repository.Interface;
using GSW.CE.Cadastro.Domain.Entity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GSW.CE.Cadastro.Data.Repository
{
    public class ClienteRepository : IClienteRepository
    {
        private readonly CadastroContext _cadastroContext;
        public ClienteRepository(CadastroContext cadastroContext)
        {
            _cadastroContext = cadastroContext;
        }

        public async Task SaveAsync(Cliente cliente)
        {
            if (cliente.Id == Guid.Empty)
            {
                _cadastroContext.Clientes.Add(cliente);
            }
            else
            {
                var savedCliente = _cadastroContext.Clientes.AsNoTracking().Include(c => c.Conta).FirstOrDefault(c => c.Id == cliente.Id);
                savedCliente = cliente;

                _cadastroContext.Update(savedCliente);
            }

            await _cadastroContext.SaveChangesAsync();
        }

        public async Task<Conta> ObterContaPeloCliente(Guid clienteId)
        {
            var cliente = await _cadastroContext.Clientes.AsNoTracking().FirstOrDefaultAsync(x => x.Id == clienteId);
            return await _cadastroContext.Contas.AsNoTracking().FirstAsync(x => x.Id == cliente.ContaId);
        }

        public async Task<IEnumerable<Cliente>> ObterClientes()
        {
            return await _cadastroContext.Clientes
                                         .AsNoTracking()
                                         .Include(x => x.Conta)
                                         .ToListAsync();
        }

        public async Task<Cliente> ObterCliente(Guid clienteId)
        {
            return await _cadastroContext.Clientes
                                         .AsNoTracking()
                                         .Include(x => x.Conta)
                                         .FirstOrDefaultAsync(x => x.Id == clienteId);
        }
    }
}
