﻿using GSW.CE.Shared.Domain.Entity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace GSW.CE.Shared.Data.Context
{
    public class AutenticacaoContext : DbContext
    {
        public AutenticacaoContext(DbContextOptions<AutenticacaoContext> options) : base(options) { }

        public DbSet<Usuario> Usuarios { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfigurationsFromAssembly(typeof(AutenticacaoContext).Assembly);
        }
    }
}
