﻿using GSW.CE.Core.Data.Interface;
using GSW.CE.Shared.Domain.Entity;
using System;
using System.Threading.Tasks;

namespace GSW.CE.Shared.Data.Repository.Interface
{
    public interface IAutenticacaoRepository : IRepository<Usuario>
    {
        Task<Usuario> ObterPorLogin(string login, string senha);
        Task<Usuario> ObterUsuario(Guid usuarioId);
    }
}
