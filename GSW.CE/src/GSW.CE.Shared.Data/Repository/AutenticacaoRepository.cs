﻿using GSW.CE.Core.Cryptography;
using GSW.CE.Shared.Data.Context;
using GSW.CE.Shared.Data.Repository.Interface;
using GSW.CE.Shared.Domain.Entity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Threading.Tasks;

namespace GSW.CE.Shared.Data.Repository
{
    public class AutenticacaoRepository : IAutenticacaoRepository
    {
        private readonly AutenticacaoContext _autenticacaoContext;
        public AutenticacaoRepository(AutenticacaoContext autenticacaoContext)
        {
            _autenticacaoContext = autenticacaoContext;
        }

        public async Task SaveAsync(Usuario usuario)
        {
            var exists = await _autenticacaoContext.Usuarios.AsNoTracking().FirstOrDefaultAsync(x => x.Id == usuario.Id) != null;

            if (!exists)
            {
                _autenticacaoContext.Usuarios.Add(usuario);
            }
            else
            {
                _autenticacaoContext.Usuarios.Update(usuario);
            }

            await _autenticacaoContext.SaveChangesAsync();
        }

        public async Task<Usuario> ObterPorLogin(string login, string senha)
        {
            var senhaCrypt = MD5Crypt.Generate(senha);
            var usuario = await _autenticacaoContext.Usuarios
                                .AsNoTracking()
                                .FirstOrDefaultAsync(x => x.Login == login && x.Senha == senhaCrypt);

            return usuario;
        }

        public async Task<Usuario> ObterUsuario(Guid usuarioId)
        {
            return await _autenticacaoContext.Usuarios
                                .AsNoTracking()
                                .FirstOrDefaultAsync(x => x.Id == usuarioId);
        }
    }
}
