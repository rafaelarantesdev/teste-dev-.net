﻿using GSW.CE.Shared.Domain.Entity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace GSW.CE.Cadastro.Data.Mapping
{
    public abstract class ContaBaseMapping<T> where T : ContaBase
    {
        public void Configure(EntityTypeBuilder<T> builder)
        {
            builder.HasKey(x => x.Id);

            builder.OwnsOne(x => x.DadosConta, y =>
            {
                y.Property(p => p.Agencia)
                    .HasColumnName("Agencia")
                    .HasColumnType("varchar(5)");

                y.Property(p => p.Numero)
                    .HasColumnName("Numero")
                    .HasColumnType("int");

                y.Property(p => p.Senha)
                    .HasColumnName("Senha")
                    .HasColumnType("varchar(32)");
            });

            builder.OwnsOne(x => x.Saldo, y =>
            {
                y.Property(p => p.Valor)
                    .HasColumnName("Saldo")
                    .HasColumnType("money");
            });

            builder.ToTable("Conta");
        }

        public virtual void AddCustomConfig(EntityTypeBuilder<T> builder)
        {
        }
    }
}
