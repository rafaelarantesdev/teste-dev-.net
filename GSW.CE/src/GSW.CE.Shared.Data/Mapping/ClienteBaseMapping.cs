﻿using GSW.CE.Shared.Domain.Entity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace GSW.CE.Shared.Data.Mapping
{
    public abstract class ClienteBaseMapping<T> where T: ClienteBase
    {
        public void Configure(EntityTypeBuilder<T> builder)
        {
            builder.HasKey(x => x.Id);

            builder.OwnsOne(x => x.CPF, y =>
            {
                y.Property(p => p.Numero)
                    .HasColumnName("CPF")
                    .HasColumnType("varchar(11)");
            });

            builder.OwnsOne(x => x.DataNascimento, y =>
            {
                y.Property(p => p.Data)
                    .HasColumnName("DataNascimento")
                    .HasColumnType("date");
            });

            builder.OwnsOne(x => x.NomeCompleto, y =>
            {
                y.Property(p => p.Nome)
                    .HasColumnName("Nome")
                    .HasColumnType("varchar(50)");

                y.Property(p => p.Sobrenome)
                    .HasColumnName("Sobrenome")
                    .HasColumnType("varchar(50)");
            });

            AddCustomConfig(builder);

            builder.ToTable("Cliente");
        }

        public virtual void AddCustomConfig(EntityTypeBuilder<T> builder)
        {
        }
    }
}
