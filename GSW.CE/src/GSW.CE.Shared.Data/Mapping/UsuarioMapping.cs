﻿
using GSW.CE.Shared.Domain.Entity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace GSW.CE.Shared.Data.Mapping
{
    public class UsuarioMapping : IEntityTypeConfiguration<Usuario>
    {
        public void Configure(EntityTypeBuilder<Usuario> builder)
        {
            builder.HasKey(x => x.Id);
            builder.Property(x => x.Permissao).IsRequired();
            builder.Property(x => x.Login).IsRequired();
            builder.Property(x => x.Senha).IsRequired();
            builder.ToTable("Usuario");
        }
    }
}
