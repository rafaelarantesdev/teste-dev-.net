﻿using Microsoft.AspNetCore.Mvc.ModelBinding;
using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;

namespace GSW.CE.Shared.Application.ViewModel
{
    public class AutenticacaoUsuarioVModel
    {
        [Required(ErrorMessage="{0} é obrigatório", AllowEmptyStrings = false)]
        public string Login { get; set; }
        [Required(ErrorMessage = "{0} é obrigatória", AllowEmptyStrings = false)]
        public string Senha { get; set; }
    }
}
