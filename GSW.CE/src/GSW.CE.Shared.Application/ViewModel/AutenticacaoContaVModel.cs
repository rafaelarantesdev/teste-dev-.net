﻿
using System.ComponentModel.DataAnnotations;

namespace GSW.CE.Shared.Application.ViewModel
{
    public class AutenticacaoContaVModel
    {
        [Required(ErrorMessage = "{0} é obrigatória")]
        [Range(10000, 99999, ErrorMessage = "{0} precisa ter 5 digitos")]
        public int Agencia{ get; set; }

        [Range(100000, 999999, ErrorMessage = "{0} precisa ter 6 digitos")]
        [Required(ErrorMessage = "{0} é obrigatória")]
        public int Conta { get; set; }

        [Required(ErrorMessage = "{0} é obrigatória")]
        [Range(100000, 999999, ErrorMessage = "{0} precisa ter 6 dígitos")]
        public int Senha { get; set; }
    }
}
