﻿using GSW.CE.Shared.Application.Service.Interface;
using GSW.CE.Shared.Data.Repository.Interface;
using GSW.CE.Shared.Domain.Entity;
using Microsoft.IdentityModel.Tokens;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Threading.Tasks;

namespace GSW.CE.Shared.Application.Service
{
    public class AutenticacaoAppService : IAutenticacaoAppService
    {
        private readonly IAutenticacaoRepository _autenticacaoRepository;

        public AutenticacaoAppService(IAutenticacaoRepository autenticacaoRepository)
        {
            _autenticacaoRepository = autenticacaoRepository;
        }

        public async Task<Usuario> Login(string login, string senha, byte[] securityKey)
        {
            var usuario  = await _autenticacaoRepository.ObterPorLogin(login, senha);

            if(usuario != null)
            {
                var tokenHandler = new JwtSecurityTokenHandler();

                var tokenDescriptor = new SecurityTokenDescriptor
                {
                    Subject = new ClaimsIdentity(new Claim[]
                    {
                        new Claim(ClaimTypes.Name, usuario.Login),
                        new Claim("CE", usuario.Permissao )
                    }),
                    Expires = DateTime.UtcNow.AddHours(1),
                    SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(securityKey), SecurityAlgorithms.HmacSha256Signature)
                };

                var token = tokenHandler.CreateToken(tokenDescriptor);
                usuario.AdicionarToken(tokenHandler.WriteToken(token));
            }

            return usuario;
        }
    }
}
