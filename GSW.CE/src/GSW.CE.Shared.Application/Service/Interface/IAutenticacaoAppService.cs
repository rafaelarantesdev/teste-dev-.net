﻿using GSW.CE.Shared.Domain.Entity;
using System.Threading.Tasks;

namespace GSW.CE.Shared.Application.Service.Interface
{
    public interface IAutenticacaoAppService
    {
        Task<Usuario> Login(string login, string senha, byte[] securityKey);
    }
}
