﻿
namespace GSW.CE.Caixa.Application.ViewModel
{
    public class DadosClienteVModel
    {
        public decimal ValorSaldo { get; set; }
        public int CodigoAgencia { get; set; }
        public int NumeroConta { get; set; }
        public string Nome { get; set; }
        public string Sobrenome { get; set; }
    }
}
