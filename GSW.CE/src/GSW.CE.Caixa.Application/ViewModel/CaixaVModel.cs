﻿using System;
using System.ComponentModel.DataAnnotations;

namespace GSW.CE.Caixa.Application.ViewModel
{
    public class CaixaVModel
    {
        [Required(ErrorMessage = "{0} é obrigatório")]
        public Guid ClienteId { get; set; }
        [Required(ErrorMessage = "{0} é obrigatório")]
        public decimal Valor { get; set; }
    }
}
