﻿using AutoMapper;
using GSW.CE.Caixa.Application.ViewModel;
using GSW.CE.Caixa.Domain.Entity;

namespace GSW.CE.Caixa.Application.AutoMapper
{
    public class CaixaDomainToViewModelMappingProfile : Profile
    {
        public CaixaDomainToViewModelMappingProfile()
        {
            CreateMap<Cliente, DadosClienteVModel>()
                .ForMember(x => x.CodigoAgencia, y => y.MapFrom(z => z.Conta.DadosConta.Agencia))
                .ForMember(x => x.NumeroConta, y => y.MapFrom(z => z.Conta.DadosConta.Numero))
                .ForMember(x => x.ValorSaldo, y => y.MapFrom(z => z.Conta.Saldo.Valor))
                .ForMember(x => x.Nome, y => y.MapFrom(z => z.NomeCompleto.Nome))
                .ForMember(x => x.Sobrenome, y => y.MapFrom(z => z.NomeCompleto.Sobrenome));
        }
    }
}
