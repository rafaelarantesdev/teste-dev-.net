﻿using GSW.CE.Caixa.Application.ViewModel;
using GSW.CE.Caixa.Domain.Interface;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GSW.CE.Caixa.Application.Service.Interface
{
    public interface ICaixaAppService
    {
        Task<IEnumerable<INota>> Sacar(CaixaVModel saqueVModel);
        Task<DadosClienteVModel> ObterDadosCliente(Guid clienteId);
    }
}
