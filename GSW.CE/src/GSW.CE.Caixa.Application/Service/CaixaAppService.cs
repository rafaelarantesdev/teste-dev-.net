﻿using AutoMapper;
using FluentValidation;
using GSW.CE.Caixa.Application.Service.Interface;
using GSW.CE.Caixa.Application.ViewModel;
using GSW.CE.Caixa.Data.Repository.Interface;
using GSW.CE.Caixa.Domain.Entity;
using GSW.CE.Caixa.Domain.Interface;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GSW.CE.Caixa.Application.Service
{
    public class CaixaAppService : ICaixaAppService
    {
        private readonly ICaixaRepository _caixaRepository;
        private readonly IMapper _mapper;

        public CaixaAppService(ICaixaRepository caixaRepository, IMapper mapper)
        {
            _caixaRepository = caixaRepository;
            _mapper = mapper;
        }

        public async Task<IEnumerable<INota>> Sacar(CaixaVModel saqueVModel)
        {
            var cliente = await _caixaRepository.ObterClientePeloId(saqueVModel.ClienteId);

            if (cliente == null)
                throw new ValidationException("Cliente inexistente");

            var caixa = new CaixaEletronico(cliente.Conta);
            var notas = caixa.Sacar(saqueVModel.Valor);

            var validacao = cliente.Conta.Validar();
            if (!validacao.IsValid)
                throw new ValidationException(validacao.Errors);

            await _caixaRepository.SaveAsync(cliente.Conta);

            return notas;
        }

        public async Task<DadosClienteVModel> ObterDadosCliente(Guid clienteId)
        {
            var cliente = await _caixaRepository.ObterClientePeloId(clienteId);

            if (cliente == null)
                throw new ValidationException("Cliente inexistente");

            return _mapper.Map<DadosClienteVModel>(cliente);
        }
    }
}
