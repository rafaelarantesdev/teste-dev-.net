﻿using AutoMapper;
using GSW.CE.Cadastro.Application.ViewModel;
using GSW.CE.Cadastro.Domain.Entity;

namespace GSW.CE.Cadastro.Application.AutoMapper
{
    public class CadastroDomainToViewModelMappingProfile : Profile
    {
        public CadastroDomainToViewModelMappingProfile()
        {
            CreateMap<Cliente, ClienteVModel>()
                .ForMember(x => x.NumeroCPF, y => y.MapFrom(z => z.CPF.Numero))
                .ForMember(x => x.DataDeNascimento, y => y.MapFrom(z => z.DataNascimento.Data))
                .ForMember(x => x.Nome, y => y.MapFrom(z => z.NomeCompleto.Nome))
                .ForMember(x => x.Sobrenome, y => y.MapFrom(z => z.NomeCompleto.Sobrenome))
                .ForMember(x => x.Agencia, y => y.MapFrom(z => z.Conta.DadosConta.Agencia))
                .ForMember(x => x.SaldoBancario, y => y.MapFrom(z => z.Conta.Saldo.Valor));
        }
    }
}
