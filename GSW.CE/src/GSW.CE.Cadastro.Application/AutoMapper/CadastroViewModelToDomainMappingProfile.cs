﻿using AutoMapper;
using GSW.CE.Cadastro.Application.ViewModel;
using GSW.CE.Cadastro.Domain.Entity;
using GSW.CE.Shared.Domain.ValueObject;
using System;

namespace GSW.CE.Cadastro.Application.AutoMapper
{
    public class CadastroViewModelToDomainMappingProfile : Profile
    {
        public CadastroViewModelToDomainMappingProfile()
        {
            CreateMap<ClienteVModel, Cliente>()
                .ConstructUsing(x =>
                    new Cliente(new NomeCompleto(x.Nome, x.Sobrenome),
                                new DataNascimento(x.DataDeNascimento),
                                new CPF(x.NumeroCPF),
                                new Conta(x.ContaId, new DadosConta(x.Agencia), new Saldo(x.SaldoBancario))
                               ));


            CreateMap<GravarClienteVModel, Cliente>()
                .ConstructUsing(x =>
                    new Cliente(new NomeCompleto(x.Nome, x.Sobrenome),
                                new DataNascimento(x.DataDeNascimento),
                                new CPF(x.NumeroCPF),
                                new Conta(x.ContaId, new DadosConta(x.Agencia).AlterarSenha(x.Senha.ToString()), new Saldo(x.SaldoBancario))
                               ));
        }
    }
}
