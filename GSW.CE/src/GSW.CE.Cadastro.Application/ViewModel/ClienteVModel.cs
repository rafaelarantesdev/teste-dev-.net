﻿
using System;
using System.ComponentModel.DataAnnotations;

namespace GSW.CE.Cadastro.Application.ViewModel
{
    public class ClienteVModel
    {
        public Guid Id { get; set; }

        [Required(ErrorMessage = "{0} é obrigatório")]
        public string Nome { get; set; }

        [Required(ErrorMessage = "{0} é obrigatório")]
        public string Sobrenome { get; set; }

        [Required(ErrorMessage = "{0} é obrigatória")]
        public DateTime DataDeNascimento { get; set; }

        [Required(ErrorMessage = "{0} é obrigatório")]
        public string NumeroCPF { get; set; }

        [Required(ErrorMessage = "{0} é obrigatório")]
        public decimal SaldoBancario { get; set; }

        public Guid ContaId { get; set; }

        [Required(ErrorMessage = "{0} é obrigatória")]
        [Range(10000, 99999, ErrorMessage = "{0} precisa ter 5 digitos")]
        public int Agencia { get; set; }
    }
}
