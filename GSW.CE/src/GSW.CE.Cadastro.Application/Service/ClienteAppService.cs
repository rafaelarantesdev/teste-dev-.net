﻿using AutoMapper;
using FluentValidation;
using GSW.CE.Cadastro.Application.Service.Interface;
using GSW.CE.Cadastro.Application.ViewModel;
using GSW.CE.Cadastro.Data.Repository.Interface;
using GSW.CE.Cadastro.Domain.Entity;
using GSW.CE.Shared.Data.Repository.Interface;
using GSW.CE.Shared.Domain.Entity;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GSW.CE.Cadastro.Application.Service
{
    public class ClienteAppService : IClienteAppService
    {
        private readonly IClienteRepository _clienteRepository;
        private readonly IMapper _mapper;
        private readonly IAutenticacaoRepository _autenticacaoRepository;

        public ClienteAppService(IClienteRepository clienteRepository, IMapper mapper,
                                 IAutenticacaoRepository autenticacaoRepository)
        {
            _clienteRepository = clienteRepository;
            _mapper = mapper;
            _autenticacaoRepository = autenticacaoRepository;
        }

        public async Task<IEnumerable<ClienteVModel>> ObterClientes()
        {
            return _mapper.Map<IEnumerable<ClienteVModel>>(await _clienteRepository.ObterClientes());
        }

        public async Task<ClienteVModel> ObterCliente(Guid clienteId)
        {
            return _mapper.Map<ClienteVModel>(await _clienteRepository.ObterCliente(clienteId));
        }

        public async Task Salvar(GravarClienteVModel clienteVModel)
        {
            var cliente = _mapper.Map<Cliente>(clienteVModel);
            var usuario = new Usuario();

            if (cliente.Id != Guid.Empty)
            {
                var savedCliente = await _clienteRepository.ObterCliente(cliente.Id);
                cliente.Conta.DadosConta.AdicionarNumeroConta(savedCliente.Conta.DadosConta.Numero);
                cliente.Conta.AssociarUsuario(savedCliente.Conta.UsuarioId);
                usuario = await _autenticacaoRepository.ObterUsuario(savedCliente.Conta.UsuarioId);
            }

            cliente.Conta.AssociarUsuario(usuario.Id);

            cliente = await SalvarCliente(cliente);

            await SalvarUsuario(cliente, usuario);
        }

        private async Task<Cliente> SalvarCliente(Cliente cliente)
        {
            var validacaoCliente = cliente.Validar();
            if (!validacaoCliente.IsValid)
                throw new ValidationException(validacaoCliente.Errors);

            await _clienteRepository.SaveAsync(cliente);

            return await _clienteRepository.ObterCliente(cliente.Id);
        }

        private async Task SalvarUsuario(Cliente cliente, Usuario usuario)
        {
            usuario.AlterarSenha(cliente.Conta.DadosConta.Senha.ToString());
            usuario.AlterarLogin($"{cliente.Conta.DadosConta.Agencia}{cliente.Conta.DadosConta.Numero}");

            var validacaoUsuario = usuario.Validar();
            if (!validacaoUsuario.IsValid)
                throw new ValidationException(validacaoUsuario.Errors);

            await _autenticacaoRepository.SaveAsync(usuario);
        }
    }
}
