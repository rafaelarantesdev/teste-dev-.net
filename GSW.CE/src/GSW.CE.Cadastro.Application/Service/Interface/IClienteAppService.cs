﻿
using GSW.CE.Cadastro.Application.ViewModel;
using GSW.CE.Cadastro.Domain.Entity;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GSW.CE.Cadastro.Application.Service.Interface
{
    public interface IClienteAppService
    {
        Task Salvar(GravarClienteVModel cliente);
        Task<IEnumerable<ClienteVModel>> ObterClientes();
        Task<ClienteVModel> ObterCliente(Guid clienteId);
    }
}
