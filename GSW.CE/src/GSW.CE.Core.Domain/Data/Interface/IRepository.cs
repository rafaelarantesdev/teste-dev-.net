﻿using System.Threading.Tasks;

namespace GSW.CE.Core.Data.Interface
{
    public interface IRepository<T>
    {
        Task SaveAsync(T entity);
    }
}
