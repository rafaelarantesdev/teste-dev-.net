﻿using FluentValidation.Results;
using System;

namespace GSW.CE.Core.DomainObject
{
    public abstract class EntityBase
    {
        public Guid Id { get; protected set; }

        protected EntityBase()
        {
            Id = Guid.NewGuid();
        }

        public abstract ValidationResult Validar();
    }
}
