﻿
namespace GSW.CE.Core.Cryptography
{
    public static class MD5Crypt
    {
        public static string Generate(string value)
        {
            var md5 = System.Security.Cryptography.MD5.Create();
            var inputBytes = System.Text.Encoding.ASCII.GetBytes(value);
            var hash = md5.ComputeHash(inputBytes);
            var sb = new System.Text.StringBuilder();

            for (int i = 0; i < hash.Length; i++)
            {
                sb.Append(hash[i].ToString("X2"));
            }
            return sb.ToString();
        }
    }
}
