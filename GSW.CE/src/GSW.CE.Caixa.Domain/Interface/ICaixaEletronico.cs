﻿
using System.Collections.Generic;

namespace GSW.CE.Caixa.Domain.Interface
{
    public interface ICaixaEletronico
    {
        IEnumerable<INota> Sacar(decimal valor);
    }
}
