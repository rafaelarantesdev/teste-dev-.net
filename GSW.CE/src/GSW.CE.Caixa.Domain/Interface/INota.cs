﻿
namespace GSW.CE.Caixa.Domain.Interface
{
    public interface INota
    {
        decimal ObterValor();
    }
}
