﻿
namespace GSW.CE.Caixa.Domain.Interface
{
    public interface IConta
    {
        void Debitar(decimal valor);
    }
}
