﻿
using GSW.CE.Caixa.Domain.Interface;

namespace GSW.CE.Caixa.Domain.Entity
{
    public class Nota : INota
    {
        private readonly decimal _valor;

        public Nota(decimal valor)
        {
            _valor = valor;
        }

        public decimal ObterValor()
        {
            return _valor;
        }
    }
}
