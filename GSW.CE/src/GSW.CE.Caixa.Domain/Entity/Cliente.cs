﻿using FluentValidation.Results;
using GSW.CE.Caixa.Domain.Entity.Validation;
using GSW.CE.Shared.Domain.Entity;
using GSW.CE.Shared.Domain.ValueObject;
using System;

namespace GSW.CE.Caixa.Domain.Entity
{
    public class Cliente : ClienteBase
    {
        public Conta Conta { get; private set; }

        protected Cliente() {}

        public Cliente(NomeCompleto nomeCompleto, DataNascimento dataNascimento, CPF cpf)
        {
            Id = Guid.NewGuid();
            NomeCompleto = nomeCompleto;
            DataNascimento = dataNascimento;
            CPF = cpf;
        }

        public override ValidationResult Validar()
        {
            return new ClienteValidation().Validate(this);
        }
    }
}
