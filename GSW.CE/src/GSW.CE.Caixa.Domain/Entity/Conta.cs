﻿using FluentValidation.Results;
using GSW.CE.Caixa.Domain.Entity.Validation;
using GSW.CE.Caixa.Domain.Interface;
using GSW.CE.Shared.Domain.Entity;
using GSW.CE.Shared.Domain.ValueObject;
using System;

namespace GSW.CE.Caixa.Domain.Entity
{
    public class Conta : ContaBase, IConta
    {
        public Cliente Cliente { get; private set; }

        protected Conta() {}

        public Conta(Cliente cliente)
        {
            Cliente = cliente;
        }

        public void Debitar(decimal valor)
        {
            Saldo.Debitar(valor);
        }

        public override ValidationResult Validar()
        {
            return new ContaValidation().Validate(this);
        }
    }
}
