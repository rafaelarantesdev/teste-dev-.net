﻿using FluentValidation;
using GSW.CE.Shared.Domain.Entity.Validation;

namespace GSW.CE.Caixa.Domain.Entity.Validation
{
    public class ContaValidation : ContaBaseValidation
    {
        public override void AddCustomConfig()
        {
            RuleFor(x => x.Saldo.Valor)
                .GreaterThanOrEqualTo(0)
                .WithMessage("Saldo indisponível para saque");
            base.AddCustomConfig();
        }
    }
}
