﻿using FluentValidation;
using FluentValidation.Results;
using GSW.CE.Core.DomainObject;
using GSW.CE.Caixa.Domain.Interface;
using System;
using System.Collections.Generic;
using System.Linq;

namespace GSW.CE.Caixa.Domain.Entity
{
    public class CaixaEletronico : ICaixaEletronico
    {
        private readonly IConta _conta;
        private readonly IList<INota> _notas = new List<INota>();
        

        public CaixaEletronico(IConta conta)
        {
            _conta = conta;
            _notas.Add(new Nota(100));
            _notas.Add(new Nota(50));
            _notas.Add(new Nota(20));
            _notas.Add(new Nota(10));
        }

        public IEnumerable<INota> Sacar(decimal valor)
        {
            if(valor <= 0)
                throw new ValidationException("Valor inválido");

            var notas = ObterNotasPeloValor(valor);
            _conta.Debitar(valor);

            return notas;
        }

        private IEnumerable<INota> ObterNotasPeloValor(decimal valorOriginal)
        {
            var erros = new List<ValidationFailure>();
            var notasUtilizadas = new List<INota>();
            var valor = valorOriginal;

            if (_notas.Count() > 0)
            {
                foreach(var nota in _notas)
                {
                    if (valor == 0) break;

                    while (valor >= nota.ObterValor())
                    {
                        notasUtilizadas.Add(nota);
                        valor -= nota.ObterValor();
                    }
                }
            }
            else
            {
                throw new ValidationException("Não há notas disponíveis");
            }

            if (notasUtilizadas.Sum(x => x.ObterValor()) != valorOriginal)
            {
                throw new ValidationException("Não é possível sacar esse valor");
            }

            return notasUtilizadas;
        }
    }
}
