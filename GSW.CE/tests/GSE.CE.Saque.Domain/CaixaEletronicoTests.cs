using FluentValidation;
using GSE.CE.Caixa.Domain.Tests.Mock;
using GSW.CE.Caixa.Domain.Entity;
using GSW.CE.Shared.Domain.ValueObject;
using System;
using System.Linq;
using Xunit;

namespace GSE.CE.Caixa.Domain.Tests
{
    public class CaixaEletronicoTests
    {
        private Cliente CriarClienteMock()
        {
            return new Cliente(
                new NomeCompleto("Rafael", "Arantes"),
                new DataNascimento(DateTime.Parse("1994-10-27")),
                new CPF("414.446.338-99")
             );
        }

        private ContaMock CriarContaMock(Cliente cliente)
        {
            return new ContaMock(cliente);
        }

        [Theory]
        [InlineData(100)]
        [InlineData(120)]
        [InlineData(130)]
        [InlineData(1000)]
        [InlineData(10)]
        public void SacarValoresPermitidos(decimal valorParaSaque)
        {

            var conta = CriarContaMock(CriarClienteMock());
            conta.Depositar(valorParaSaque);

            var caixa = new CaixaEletronico(conta);

            var notasSacadas = caixa.Sacar(valorParaSaque);
            var valoresNotasSacadas = notasSacadas.Sum(n => n.ObterValor());

            Assert.Equal(valorParaSaque, valoresNotasSacadas);
        }

        [Fact]
        public void TentarSacarValorNegativo()
        {

            var conta = CriarContaMock(CriarClienteMock());
            conta.Depositar(100);

            var caixa = new CaixaEletronico(conta);

            Assert.Throws<ValidationException>(() =>
                caixa.Sacar(-10)
            );
        }

        [Fact]
        public void TentarSacarValorMaiorDoQueOSaldo()
        {

            var conta = CriarContaMock(CriarClienteMock());
            conta.Depositar(100);

            var caixa = new CaixaEletronico(conta);
            caixa.Sacar(1000);
            var validacao = conta.Validar();

            Assert.False(validacao.IsValid);
        }

        [Fact]
        public void TentarSacarComNotaIndisponivel()
        {

            var conta = CriarContaMock(CriarClienteMock());
            conta.Depositar(5);

            var caixa = new CaixaEletronico(conta);

            Assert.Throws<ValidationException>(() =>
                caixa.Sacar(5)
            );
        }

        [Theory]
        [InlineData(100, 1)]
        [InlineData(120, 2)]
        [InlineData(130, 3)]
        [InlineData(1000, 10)]
        [InlineData(10, 1)]
        [InlineData(170, 3)]
        public void SacarComMenorNumeroDeNotas(decimal valorParaSaque, int qtdNotas)
        {

            var conta = CriarContaMock(CriarClienteMock());
            conta.Depositar(valorParaSaque);

            var caixa = new CaixaEletronico(conta);

            var notasSacadas = caixa.Sacar(valorParaSaque);

            Assert.Equal(qtdNotas, notasSacadas.Count());
        }
    }
}
