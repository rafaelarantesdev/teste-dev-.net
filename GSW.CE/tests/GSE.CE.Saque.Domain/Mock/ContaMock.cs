﻿using GSW.CE.Caixa.Domain.Entity;
using GSW.CE.Shared.Domain.ValueObject;
using System;

namespace GSE.CE.Caixa.Domain.Tests.Mock
{
    public class ContaMock : Conta
    {
        public ContaMock(Cliente cliente) : base(cliente)
        {
            Saldo = new Saldo(0);
            DadosConta = new DadosConta(11111);
            DadosConta.AdicionarNumeroConta(111111);
        }

        public void Depositar(decimal valor)
        {
            Saldo.Depositar(valor);
        }
    }
}
