﻿using GSW.CE.Cadastro.Domain.Entity;
using GSW.CE.Shared.Domain.ValueObject;
using System;
using Xunit;

namespace GSW.CE.Cadastro.Domain.Tests
{
    public class CadastroClienteTests
    {
        private Cliente ObterCliente()
        {
            return new Cliente(
                new NomeCompleto("Nelson", "Ramos"),
                new DataNascimento(DateTime.Parse("1958-07-21")),
                new CPF("209.121.287-39"),
                new Conta(Guid.Empty, new DadosConta(11111), new Saldo(150))
            );
        }

        public Conta ObterConta()
        {
            return new Conta(Guid.Empty, new DadosConta(15581).AdicionarNumeroConta(111111), new Saldo(100));
        }

        [Fact]
        public void CriarClienteValido()
        {
            var cliente = ObterCliente();

            Assert.True(cliente.Validar().IsValid);
        }

        [Fact]
        public void CriarClienteInValido()
        {
            var cliente = new Cliente(
                new NomeCompleto("", ""),
                new DataNascimento(DateTime.Parse("1958-07-21")),
                new CPF("209.121.287-39"),
                new Conta(Guid.Empty, new DadosConta(1), new Saldo(-1))
            );

            Assert.False(cliente.Validar().IsValid);
        }

        [Fact]
        public void CriarContaValida()
        {
            var conta = ObterConta();
            Assert.True(conta.Validar().IsValid);
        }

        [Fact]
        public void CriarContaInValida()
        {
            var conta = new Conta(Guid.Empty, new DadosConta(111).AdicionarNumeroConta(1), new Saldo(-1));
            Assert.False(conta.Validar().IsValid);
        }
    }
}
